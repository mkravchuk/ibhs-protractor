/**
 * Created by marta on 02.02.15.
 */

var Settings = function() {
// Text For Search Patient :
   this.patientName = 'Cynthia Debra Alexis';

//Text For Add Note :
   this.template = 'Marta';
   this.comment = 'bla-bla';

// Not Required Data For New Patient :
    this.middleNamePatient = 'Augusta';
    this.ssnPatient = '810-38-4132';

// Messages And Confirmations :
    this.actualConfirmationPopUpOnDeletion = 'Are you sure you want to delete';
    this.actualSuccessfulMessage = 'was successfully deleted';
    this.actualMessageAboutConfirmationActionOfCancellation = 'Are you sure you want to leave? All unsaved changes will be lost.';
    this.notificationAboutSuccessfulSaving = 'Changes have been successfully saved!';
    this.messageAfterSearching = 'No results found...';

//Text For filling fields on NewPatient Page :
    this.firstNamePatient = 'Primary';
    this.lastNamePatient = 'Insurance';

// Text for filling AddressInformation :
    this.address = '5400 Broad Zephyr Trace';
    this.city = 'Quarantine';
    this.state = 'DE';
    this.zipCode = '80560-0908';

// text for filling fields in Insurance Tabs on NewPatient Page :
    this.memberId = '12345678';
    this.patientsRelationshipToInsured = 'Spouse';
    this.firstNamePatientPolicyHolder = 'Paula';
    this.lastNamePatientPolicyHolder = 'Ronnie';


};
module.exports = new Settings();

/*var NotRequiredDataForNewPatient = function() {
    this.middleNamePatient = 'Augusta';
    this.ssnPatient = '810-38-4132';
};
module.exports = new NotRequiredDataForNewPatient();*/


