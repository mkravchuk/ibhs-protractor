/**
 * Created by marta on 18.02.15.
 */

var UniqueLocator = function() {
    this.searchFieldForListContainer = element(by.xpath('//div[contains(@class,"open")]//div[@class="dropdown-menu open"]//input'));
    this.listContainer = element.all(by.xpath('//div[contains(@class,"open")]/div[@class="dropdown-menu open"]//li[not(@data-original-index="0")]'));
    this.buttonOK = element(by.xpath('//div[@class="modal-dialog"]//button[contains(text(),"OK")]'));
    this.buttonOk = element(by.xpath('//div[@class="modal-dialog"]//button[contains(text(),"Ok")]'));
    this.linkThatDeterminateFacility = element(by.xpath('//div[@class="navbar-right"]//a[contains(@ng-click, "updateFacility")]'));
    this.buttonYes = element(by.xpath('//div[@class="modal-dialog"]//button[contains(@ng-click, "ok") and contains(text(), "Yes")]'));
    this.buttonNo = element(by.xpath('//div[@class="modal-dialog"]//button[@ng-click="cancel()" and contains(text(),"No")]'));
   // this.mainMenuHeader = element(by.xpath('//div[@class="container-fluid"]//div[@class="navbar-collapse collapse"]'));

    this.buttonSave = element(by.xpath('//button[contains(text(), "Save") and @type="submit"]'));

// NEW Patient/Claim/Insurance Pages :
    this.backButton = element(by.xpath('//a[contains(@class, "btn-cancel") and contains(text(),"Back")]'));
// Messages And Confirmations :
    this.confirmationAboutCancellation = element(by.xpath('//div[@class="modal-dialog"]//div[@class="modal-body"]'));
    this.notificationAboutSuccessfulSaving = element(by.xpath('//div/notification//div[contains(@class, "alert-success")]//div[@class="ng-binding"]'));
// Calendar :
    this.calendarMonthButton = element(by.xpath('//ul[contains(@style,"display: block")]//button[contains(@id, "datepicker")]'));
    this.calendarYearButton = element(by.xpath('//table[@ng-switch-when="month"]//button[contains(@id, "datepicker")]'));
    this.calendarCellsYearMonthDay = element.all(by.xpath('//ul[contains(@style,"block")]//td[contains(@class,"text-center date-picker-day ng-scope")]/button/span[contains(@class, "ng-binding") and not(contains(@class, "text-muted"))]'));
// For all Tabs on NewPatient Page :
    this.policyTypeInsuranceButton = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//select[@name="PolicyTypeId"]/..//button'));
    this.payerInsuranceButton = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//select[@name="InsuranceCompanyId"]/..//button'));
    this.memberIdInsurance = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//input[@name="MemberId"]'));
    this.buttonForSelectionPatientsRelationship = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//select[@name="HolderType"]/..//button'));
    this.firstNamePatientPolicyHolderInput = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//input[@name="HolderFirstName"]'));
    this.lastNamePatientPolicyHolderInput = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//input[@name="HolderLastName"]'));
    this.genderPolicyHolderButton = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//select[@name="HolderSex"]/..//button'));
    this.DOBPolicyHolderInput = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//date-picker[@name="HolderBirthDate"]//input'));
    this.DOBPolicyHolderButton = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//date-picker[@name="HolderBirthDate"]//button'));
    this.addressPolicyHolderInput = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//input[@id="Address_Line1" and @name="Line1"]'));
    this.cityPolicyHolderInput = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//input[@id="Address_City" and @name="City"]'));
    this.statePolicyHolderInput = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//input[@id="Address_StateCode" and @name="StateCode"]'));
    this.zipCodePolicyHolderInput = element(by.xpath('//div[contains(@class,"main-content")]/div[@class="content-tabs ng-isolate-scope"]/ul/li[contains(@class,"active")]/../../div/div[contains(@class,"active")]//ul/li[contains(@class, "active")]/../../div/div[contains(@class,"active")]//input[@id="Address_ZIP" and @name="ZIP"]'));
};
module.exports = new UniqueLocator();
