/**
 * Created by marta on 20.02.15.
 */

var PatientPage = function() {
    var text = require('../functions/generalForText.js');
    var settings = require('../settings/settings.js');
    var locator = require('../settings/uniqueLocator.js');

    this.linkPatient = element(by.xpath('//div[@class="navbar-collapse collapse"]//a[contains(text(), "Patients")]'));
    this.newPatientButton = element(by.xpath('//a[contains(text(), "New Patient")]'));

/*
NEW/EDIT PATIENT :
 */
// => NotRequiredFields :
    this.middleNameInput = element(by.xpath('//input[@id="middleName" and @name="Patient_MiddleName"]'));
    this.ssnInput = element(by.xpath('//input[@id="SSN" and @name="Patient_SSN"]'));
// => Calendar :
    this.DOBInput = element(by.xpath('//date-picker[@id="birthDate" and @name="Patient_BirthDate"]//input'));
    this.calendarDOBButton = element(by.xpath('//date-picker [@name="Patient_BirthDate"]//button'));
// => RequiredFields :
    this.firstNameInput = element(by.xpath('//input[@id="firstName" and @name="Patient_FirstName"]'));
    this.lastNameInput = element(by.xpath('//input[@id="lastName" and @name="Patient_LastName"]'));
    this.addressInput = element(by.xpath('//div[contains(@ng-model,"Patient.Address")]//input[@id="Address_Line1" and @name="Line1"]'));
    this.cityInput = element(by.xpath('//div[contains(@ng-model,"Patient.Address")]//input[@id="Address_City" and @name="City"]'));
    this.stateInput = element(by.xpath('//div[contains(@ng-model,"Patient.Address")]//input[@id="Address_StateCode" and @name="StateCode"]'));
    this.zipInput = element(by.xpath('//div[contains(@ng-model,"Patient.Address")]//input[@id="Address_ZIP" and @name="ZIP"]'));
    this.genderButton = element(by.xpath('//div[@class="tab-pane ng-scope active"]//select[@name="Patient_Sex"]/..//button'));
// => Tabs :
    this.primaryInsuranceTab = element(by.xpath('//div[@type="tabs"]//li[@heading="Primary Insurance"]/a[contains(text(), "Primary Insurance")]'));



    this.selectFullDateFromCalendarPicker = function(element, buttonCalendar, valueYear, valueMonth, valueDay) {
        element.clear()
        browser.controlFlow().execute(function(){
            buttonCalendar.click();
            browser.sleep(500)
                .then(function(){
                    locator.calendarMonthButton.click();
                    browser.sleep(500)
                        .then(function(){
                            locator.calendarYearButton.click();
                            browser.sleep(500)
                                .then(function(){
                                   /* var year = ['2001','2002','2003','2004','2005','2006','2007','2008','2009','2010',
                                        '2011','2012','2013','2014','2015','2016','2017','2018','2019','2020'];
                                    for (i = 0; i < year.length; ++i) {
                                        expect(locator.calendarCellsYearMonthDay.get(i).getText()).toEqual(year[i]);
                                        locator.calendarCellsYearMonthDay.get(12).click();
                                        browser.sleep(3000);
                                        break;
                                    }*/
                                    var desiredYear;
                                    locator.calendarCellsYearMonthDay.then(function(cellsYear){
                                        cellsYear.some(function(year){
                                            year.getText().then(function(text){
                                                if(text.indexOf(valueYear) != -1){
                                                    desiredYear = year;
                                                    browser.sleep(1000);
                                                    return true;
                                                }
                                            })
                                        })
                                    }).then(function(){
                                        if(desiredYear){
                                            desiredYear.click();
                                        }
                                    })

                                });
                            browser.controlFlow().execute(function(){
                                var desiredMonth;
                                locator.calendarCellsYearMonthDay.then(function(cellsMonth){
                                    cellsMonth.some(function(month){
                                        month.getText().then(function(text){
                                            if(text.indexOf(valueMonth) != -1){
                                                desiredMonth = month;
                                                browser.sleep(1000);
                                                return true;
                                            }
                                        })
                                    })
                                }).then(function(){
                                    if(desiredMonth){
                                        desiredMonth.click();
                                    }
                                })
                            });
                            browser.controlFlow().execute(function(){
                                var desiredDay;
                                locator.calendarCellsYearMonthDay.then(function(cellsDay){
                                    cellsDay.some(function(day){
                                        day.getText().then(function(text){
                                            if(text.indexOf(valueDay) != -1){
                                                desiredDay = day;
                                                browser.sleep(1000);
                                                return true;
                                            }
                                        })
                                    })
                                }) .then(function(){
                                        if(desiredDay){
                                            desiredDay.click();
                                        }
                                    })
                                /*locator.calendarCellsYearMonthDay.then(function(day){
                                    for (z = 0; z < day.length; ++z) {
                                        locator.calendarCellsYearMonthDay.get(valueDay).click();
                                        browser.sleep(1000);
                                        break;
                                    }
                                })*/
                            })
                        })
                })
        })
    };

    this.fillPatientDataTabWithAddressInformationData = function(){
        text.inputText(this.addressInput, settings.address);
        text.inputText(this.cityInput, settings.city);
        text.inputText(this.stateInput, settings.state);
        text.inputText(this.zipInput, settings.zipCode);
    };

    this.fillPatientDataTabWithNotRequiredData = function() {
        text.inputText(this.middleNameInput, settings.middleNamePatient);
        text.inputText(this.ssnInput, settings.ssnPatient);
    };

    this.fillInsuranceTabWithAddressInformationData = function() {
        text.inputText(locator.addressPolicyHolderInput, settings.address);
        text.inputText(locator.cityPolicyHolderInput, settings.city);
        text.inputText(locator.statePolicyHolderInput, settings.state);
        text.inputText(locator.zipCodePolicyHolderInput, settings.zipCode);
    };

};
module.exports = new PatientPage();