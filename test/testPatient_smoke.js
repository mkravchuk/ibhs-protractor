/**
 * Created by marta on 20.02.15.
 */

describe('tests for Patient Page', function() {
    var locator = require('../settings/uniqueLocator.js');
    var settings = require('../settings/settings.js');
    var loginPage = require('../pages/loginPage.js');
    var billingPage = require('../pages/billingPage.js');
    var patientPage = require('../pages/patientPage.js');

//Functions :
    var text = require('../functions/generalForText.js');
    var table = require('../functions/generalForTablesOnEachPage.js');
    var params = browser.params;

// Arrays with Unique Data :
    var uniqueLastNamePatient = [];
    var fullNameOfObjectInTable = [];
    var previousInfoBeforeEditing = [];

    beforeEach(function() {
        browser.ignoreSynchronization = true;
    });

    it('Should Be' + ' ' + params.facility.constantFacility + ' ' + 'Selected', function() {
        browser.manage().deleteAllCookies();
        browser.sleep(2000);
        browser.controlFlow().execute(function(){
            browser.get(params.mainUrls.userUrl);
            browser.sleep(6000);
        }).then(function(){
            loginPage.loginToIBHS(params.login.user, params.login.password);
            browser.sleep(2000);
        }).then(function(){
            billingPage.buttonForSelectionFacility.click();
            browser.sleep(500)
                .then(function(){
                    text.inputText(locator.searchFieldForListContainer, params.facility.constantFacility);
                    browser.sleep(500)
                        .then(function(){
                            table.selectItemFromDropDown(params.facility.constantFacility);
                            browser.sleep(500)
                                .then(function(){
                                    locator.buttonOK.click();
                                    browser.sleep(1000);
                                })
                        })
                })
        })
            .then(function() {expect(locator.linkThatDeterminateFacility.getText()).toEqual(params.facility.constantFacility);
            });
    });

    it('Should Be Patient Page Selected', function(){
        browser.actions().mouseMove(patientPage.linkPatient).click().perform();
        patientPage.linkPatient.click();
        browser.sleep(5000);

        expect(browser.getTitle()).toContain('Billing - Patients');
    });

    it('Verify If User Can Search Patient Via Patient Name Field - 1', function() {
        table.searchTableRecordViaInputField(table.nameSearchField, settings.patientName);
        browser.sleep(1000)
            .then(function(){
                table.cleanArray();
                browser.sleep(1000);
            })
        browser.controlFlow().execute(function(){
            table.addResultsToArrayWithText(table.cellsOfTable)
            browser.sleep(1000)
                .then(function(){
                    var result = table.isArrayContains(settings.patientName);
                    expect(result == true);
                    console.log('The User can search the patient');
                })
        })
    });

    it('Verify If User Can See The Result Of Search Patient - 2',function(){
        table.cleanArray();
        browser.sleep(1000)
        browser.controlFlow().execute(function(){
            table.addResultsToArray(table.rowsOfTable);
            browser.sleep(1000)
                .then(function(){
                    var result = table.printResults();
                    browser.sleep(1000);
                    expect(result > 0);
                    if(true) console.log('The User can see ' + result + ' results of search Patient : ' + settings.patientName);
                })
        })
    });

    it('Verify If User Can Reset The Found Patient - 3', function(){
        table.resetTableRecord();
        browser.sleep(1000);

        expect(table.nameSearchField.value == '');
        if(true) console.log('The User can reset the patient');
    });

    it('Verify If User Can Browse Patient List - 4', function(){
        table.scrollingToPointOfDestination(table.buttonNextOfPagination);
        browser.sleep(1000)
            .then (function(){
            table.pagination();
            browser.sleep(1000);
        })
            .then(function(){
                table.cleanArray();
                browser.sleep(1000);
            })
        browser.controlFlow().execute(function(){
            table.addResultsToArray(table.rowsOfTable);
            browser.sleep(1000)
                .then (function(){
                var result = table.printResults();
                browser.sleep(1000);
                expect(result > 0);
                if(true) console.log('The User can see : ' + result + ' results after pagination');
            })
        })
    });

    it('Verify If User Can See Patients Details From Random Table Record - 5', function(){
        table.rowsOfTable.then(function (tableRow){
            var randomItem = Math.floor((Math.random() * tableRow.length));
            browser.actions().mouseMove(tableRow[randomItem]).click().perform()
            tableRow[randomItem].click();
            browser.sleep(3000);
        })
            .then(function(){
                table.scrollingToPointOfDestination(table.notePannel);
                browser.sleep(2000)
                    .then(function(){
                        var allDetails = table.defaultClosedPannelDatails;
                        allDetails.then(function (detail) {
                            for (i = detail.length-1; i >= 0; --i) {
                                var detailItem = detail[i];
                                allDetails.get(i).click();
                                browser.sleep(1000);
                            }
                        })
                    })
            })
            .then(function(){
                table.cleanArray();
                browser.sleep(1000);
            })
        browser.controlFlow().execute(function(){
            table.addResultsToArrayWithText(table.recordPatientsDetails)
            browser.sleep(1000)
                .then(function(){
                    var result = table.printResultsWithText();
                    expect(result > 0);
                    if(true) console.log('The User can see ' + result + ' details from random selected table record');
                })
        })
    });

    it('Verify If User Can Add Note To Patient - 6', function(){
        table.scrollingToPointOfDestination(table.editButton);
        browser.sleep(1000)
            .then(function(){
                table.addClaimButton.click();
                browser.sleep(2000)
                    .then(function(){
                        table.templateButton.click();
                        browser.sleep(500)
                            .then(function(){
                                text.inputText(locator.searchFieldForListContainer, settings.template);
                                browser.sleep(500)
                                    .then(function(){
                                        table.selectItemFromDropDown(settings.template);
                                        browser.sleep(500)
                                            .then(function(){
                                                text.inputText(table.freeFormText, /*table.getRandomString(10)*/ settings.comment);
                                                browser.sleep(500)
                                                    .then(function(){
                                                        locator.buttonOk.click();
                                                        browser.sleep(5000);
                                                    })
                                            })
                                    })
                            })
                    })
            })
            .then(function(){
                table.scrollingToPointOfDestination(table.notePannel);
                browser.sleep(1000)
                    .then(function(){
                        expect(table.commentNameInNotesDetails.getText()).toEqual(settings.comment);
                    })
            })
    });

    it('Verify If User Can See Confirmation PopUp When Deletes Note From Patient - 7',function(){
        browser.actions().mouseMove(table.commentNameInNotesDetails).perform();
        browser.sleep(2000)
        browser.controlFlow().execute(function(){
            browser.actions().mouseMove(table.deleteNoteButton).click().perform();
            browser.sleep(2000)
                .then(function(){
                    //var actualConfirmationPopUpOnDeletion = 'Are you sure you want to delete';
                    expect(table.confirmationAboutDeletion.getText()).toEqual(settings.actualConfirmationPopUpOnDeletion + ' ' + settings.template + ' ' + '?');
                    browser.sleep(1000)
                })
        })
    });

    it('Verify If User Can See Successful Message When Note From Patient Is Deleted - 8', function(){
        locator.buttonYes.click();
        browser.sleep(2000)
            .then(function(){
                //var actualSuccessfulMessage = 'was successfully deleted';
                expect(table.messageAboutSuccessfulDeletion.getText()).toEqual('Note' + ' ' + settings.actualSuccessfulMessage);
                browser.sleep(1000)
            })
    });

    it('Verify If User Can Delete Note From Patient - 9', function(){
        locator.buttonOk.click();
        browser.sleep(2000)
            .then(function(){
                var actualMessageAboutAbsenceNoteInNotesDetails = 'There are no notes yet on this patient';
                expect(table.messageAboutAbsenceNoteInNotesDetails.getText()).toEqual(actualMessageAboutAbsenceNoteInNotesDetails);
                browser.sleep(1000)
            })
    });

    it('Verify If User Can Cancel Creating Of New Patient Without Making Change - 10_1',function(){
        browser.executeScript('window.scrollTo(0,0);').then(function () {
            browser.sleep(1000)
        })
            .then(function(){
                patientPage.newPatientButton.click();
                browser.sleep(5000)
                    .then(function(){
                        locator.backButton.click();
                        browser.sleep(2000)
                    })
                expect(browser.getTitle()).toContain('Billing - Patients');
            })
    });

    it('Verify If User Can See Confirmation PopUp When Go Back With Unsaved Changes - 11',function(){
        patientPage.newPatientButton.click();
        browser.sleep(5000)
            .then(function(){
                patientPage.fillPatientDataTabWithNotRequiredData();
                browser.sleep(1000)
                    .then(function(){
                        locator.backButton.click();
                        browser.sleep(1000)
                            .then(function(){
                                //var actualMessageAboutConfirmationActionOfCancellation = 'Are you sure you want to leave? All unsaved changes will be lost.';
                                expect(locator.confirmationAboutCancellation.getText()).toEqual(settings.actualMessageAboutConfirmationActionOfCancellation);
                                browser.sleep(1000)
                            })
                    })
            })
    });

    it('Verify If User Can Cancel Creating Of New Patient With Unsaved Changes - 10_2',function(){
        locator.buttonYes.click();
        browser.sleep(2000);
        expect(browser.getTitle()).toContain('Billing - Patients');
    });

    it('Verify If User Can Create New Patient Filling Mandatory Fields (With Unique LastName) + PrimaryBehavioralInsurance - 12_1',function(){
        patientPage.newPatientButton.click();
        browser.sleep(5000);
        browser.controlFlow().execute(function(){
            text.inputText(patientPage.firstNameInput, settings.firstNamePatient);
            browser.sleep(500)
                .then(function(){
                    text.inputText(patientPage.lastNameInput, settings.lastNamePatient + table.getRandomString(5));
                    browser.sleep(500)
                        .then(function(){
                            var value = patientPage.lastNameInput.getAttribute('value')
                                .then(function(text){
                                    uniqueLastNamePatient.push(text);
                                    console.log('Unique last name of Patient is remembered : => ' + uniqueLastNamePatient.toString())
                                })
                        })
                })
        }).then(function(){
            patientPage.fillPatientDataTabWithAddressInformationData();
            browser.sleep(500)
                .then(function(){
                    patientPage.selectFullDateFromCalendarPicker(patientPage.DOBInput, patientPage.calendarDOBButton, '2012','May','10');
                    browser.sleep(1000)
                        .then(function(){
                            billingPage.selectRandomItemFromDropdown(patientPage.genderButton);
                            browser.sleep(1000)
                        })
                })
            });
        browser.controlFlow().execute(function(){
            patientPage.primaryInsuranceTab.click();
            browser.sleep(500)
                .then(function(){
                    billingPage.selectRandomItemFromDropdown(locator.payerInsuranceButton);
                    browser.sleep(1000)
                        .then(function(){
                            text.inputText(locator.memberIdInsurance, settings.memberId);
                            browser.sleep(1000)
                                .then(function(){
                                    locator.buttonForSelectionPatientsRelationship.click();
                                    browser.sleep(500)
                                        .then(function(){
                                            table.selectItemFromDropDown(settings.patientsRelationshipToInsured);
                                            browser.sleep(500)
                                                .then(function(){
                                                    text.inputText(locator.firstNamePatientPolicyHolderInput, settings.firstNamePatientPolicyHolder);
                                                    browser.sleep(500)
                                                        .then(function(){
                                                            text.inputText(locator.lastNamePatientPolicyHolderInput, settings.lastNamePatientPolicyHolder);
                                                            browser.sleep(500)
                                                                .then(function(){
                                                                    billingPage.selectRandomItemFromDropdown(locator.genderPolicyHolderButton);
                                                                    browser.sleep(1000)
                                                                        .then(function(){
                                                                            patientPage.selectFullDateFromCalendarPicker(locator.DOBPolicyHolderInput, locator.DOBPolicyHolderButton, '2010','September','28');
                                                                            browser.sleep(1000)
                                                                                .then(function(){
                                                                                    patientPage.fillInsuranceTabWithAddressInformationData();
                                                                                    browser.sleep(1000)
                                                                                    })
                                                                            })
                                                                    })
                                                            })
                                                    })
                                            })
                                    })
                            })
                    })
            })
            .then(function(){
                browser.executeScript('window.scrollTo(0,0);').then(function () {
                    browser.sleep(1000)
                        .then(function(){
                            locator.buttonSave.click();
                            browser.sleep(3000);
                            expect(locator.notificationAboutSuccessfulSaving.getText()).toEqual(settings.notificationAboutSuccessfulSaving);
                        })
                })
            })
    });

    it('verify If User Can Edit Patient Record - 13',function(){
        browser.actions().mouseMove(patientPage.linkPatient).click().perform();
        patientPage.linkPatient.click();
        browser.sleep(1000)
            .then(function(){
                table.rowsOfTable.then(function (tableRow){
                    var randomItem = Math.floor((Math.random() * tableRow.length));
                    browser.actions().mouseMove(tableRow[randomItem]).click().perform()
                    tableRow[randomItem].click();
                    browser.sleep(2000)
                        .then(function(){
                            var necessaryCellFromRecord = table.cellsOfActiveTableRicord.get(1).getAttribute('title')
                                .then(function(text){
                                    fullNameOfObjectInTable.push(text);
                                    console.log('The cell with needed info from selected row is remembered : => ' + fullNameOfObjectInTable.toString())
                                })
                        }).then(function(){
                            table.editButton.click();
                            browser.sleep(7000)
                                .then(function(){
                                    var value = patientPage.addressInput.getAttribute('value')
                                        .then(function(text){
                                            previousInfoBeforeEditing.push(text);
                                            console.log('String before Editing : => ' + previousInfoBeforeEditing.toString())
                                        })
                                }).then(function(){
                                    patientPage.addressInput.clear();
                                    browser.sleep(500)
                                        .then(function(){
                                            text.inputText(patientPage.addressInput, table.getRandomString(10));
                                            browser.sleep(500)
                                        })
                                }).then(function(){
                                    locator.buttonSave.click();
                                    browser.sleep(3000);
                                    expect(locator.notificationAboutSuccessfulSaving.getText()).toEqual(settings.notificationAboutSuccessfulSaving);
                                })
                        })
                })
            })
    });

    it('verify If User Can See Patients Details After Editing From Just Editable Table Record - 14',function(){
        browser.actions().mouseMove(patientPage.linkPatient).click().perform();
        patientPage.linkPatient.click();
        browser.sleep(1000)
            .then(function(){
                table.searchTableRecordViaInputField(table.nameSearchField, fullNameOfObjectInTable.toString());
                browser.sleep(1000)
                    .then(function(){
                        table.rowsOfTable.then(function (tableRow){
                            var randomItem = Math.floor((Math.random() * tableRow.length));
                            browser.actions().mouseMove(tableRow[randomItem]).click().perform()
                            tableRow[randomItem].click();
                            browser.sleep(2000)
                                .then(function(){
                                    var tempArray = []
                                    var map = table.recordPatientsDetails.map(function(details){
                                        details.getText().then(function(text){
                                            tempArray.push(text)
                                            })
                                        });
                                    browser.controlFlow().execute(function(){
                                        var resultAfterEditing = tempArray[5].substring(0, tempArray[5].indexOf(','));
                                        console.log('String after Editing : ' + ' => ' + resultAfterEditing);
                                        expect(resultAfterEditing).not.toEqual(previousInfoBeforeEditing.toString())
                                    })
                                });
                        })
                    })
                })
    });

    it('verify If User Can Cancel Deleting Of Patient - 15',function(){
        table.resetTableRecord();
        browser.sleep(1000)
            .then(function(){
                table.searchTableRecordViaInputField(table.nameSearchField, uniqueLastNamePatient.toString());
                browser.sleep(1000)
                    .then(function(){
                        table.rowsOfTable.then(function (tableRow){
                            var randomItem = Math.floor((Math.random() * tableRow.length));
                            browser.actions().mouseMove(tableRow[randomItem]).click().perform()
                            tableRow[randomItem].click();
                            browser.sleep(2000)
                                .then(function(){
                                    while(fullNameOfObjectInTable.length > 0) {
                                        fullNameOfObjectInTable.pop()
                                    }
                                    var necessaryCellFromRecord = table.cellsOfActiveTableRicord.get(1).getAttribute('title')
                                        .then(function(text){
                                            fullNameOfObjectInTable.push(text);
                                            console.log('The cell with needed info from selected row is remembered : => ' + fullNameOfObjectInTable.toString())
                                        })
                                })
                                .then(function(){
                                    table.deleteButton.click();
                                    browser.sleep(1000)
                                        .then(function(){
                                            locator.buttonNo.click();
                                            browser.sleep(1000)
                                                .then(function(){
                                                    table.resetTableRecord();
                                                    browser.sleep(1000)
                                                        .then(function(){
                                                            table.searchTableRecordViaInputField(table.nameSearchField, fullNameOfObjectInTable.toString());
                                                            browser.sleep(1000);
                                                        });
                                                    browser.controlFlow().execute(function(){
                                                        table.addResultsToArrayWithText(table.cellsOfTable)
                                                        browser.sleep(1000)
                                                            .then(function(){
                                                                var result = table.isArrayContains(fullNameOfObjectInTable.toString());
                                                                expect(result).toBe(true);
                                                                console.log('The Patient ' + '"' + fullNameOfObjectInTable.toString() + '"' +
                                                                ' which you wanted to delete is detected, and it means that Cancel function is implemented successfully');
                                                            })
                                                    })
                                                })
                                        })
                                })
                        })
                    })
            })
    });

    it('verify If User Can See Confirmation PopUp When Deletes Patient - 16',function(){
        table.rowsOfTable.then(function (tableRow) {
            var randomItem = Math.floor((Math.random() * tableRow.length));
            browser.actions().mouseMove(tableRow[randomItem]).click().perform()
            tableRow[randomItem].click();
            browser.sleep(2000)
                .then(function() {
                    table.deleteButton.click();
                    browser.sleep(1000)
                        .then(function(){
                            expect(table.confirmationAboutDeletion.getText()).toEqual(settings.actualConfirmationPopUpOnDeletion + ' ' + fullNameOfObjectInTable.toString() + ' ' + '?');
                            browser.sleep(1000)
                        })
                })
        })
    });

    it('verify If User Can See Successful Message When Patient Is Deleted - 17', function(){
        locator.buttonYes.click();
        browser.sleep(1000)
            .then(function(){
                expect(table.messageAboutSuccessfulDeletion.getText()).toEqual('Patient' + ' ' + settings.actualSuccessfulMessage);
                browser.sleep(1000)
            })
    });

    it('verify If User Can Delete Patient - 18',function(){
        locator.buttonOk.click();
        browser.sleep(1000)
            .then(function(){
                expect(table.tableMessageWhenNoSearchResults.getText()).toEqual(settings.messageAfterSearching);
                browser.sleep(1000)
            })

    });

});
