/**
 * Created by marta on 05.03.15.
 */

describe('tests for all Pages which has dependencies',function(){
    var loginPage = require('../pages/loginPage.js');
    var billingPage = require('../pages/billingPage.js');
    var locator = require('../settings/uniqueLocator.js');
    var insurancePage = require('../pages/insurancePage.js');

    var text = require('../functions/generalForText.js');
    var table = require('../functions/generalForTablesOnEachPage.js');
    var params = browser.params;

    beforeEach(function() {
        browser.ignoreSynchronization = true;
    });

    it('Should Be' + ' ' + params.facility.constantFacility + ' ' + 'Selected', function() {
        browser.manage().deleteAllCookies();
        browser.sleep(2000);
        browser.controlFlow().execute(function(){
            browser.get(params.mainUrls.userUrl);
            browser.sleep(6000);
        }).then(function(){
            loginPage.loginToIBHS(params.login.user, params.login.password);
            browser.sleep(2000);
        }).then(function(){
            billingPage.buttonForSelectionFacility.click();
            browser.sleep(500)
                .then(function(){
                    text.inputText(locator.searchFieldForListContainer, params.facility.constantFacility);
                    browser.sleep(500)
                        .then(function(){
                            table.selectItemFromDropDown(params.facility.constantFacility);
                            browser.sleep(500)
                                .then(function(){
                                    locator.buttonOK.click();
                                    browser.sleep(1000);
                                })
                        })
                })
        })
            .then(function() {expect(locator.linkThatDeterminateFacility.getText()).toEqual(params.facility.constantFacility);
            });
    });

    it('Should Be Insurance Company Page Selected',function(){
        browser.actions().mouseMove(insurancePage.linkInsurance).click().perform();
        insurancePage.linkInsurance.click();
        browser.sleep(5000);

        expect(browser.getTitle()).toContain('Billing - Insurance Companies');
    });

})
